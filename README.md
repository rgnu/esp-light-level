How to build PlatformIO based project
=====================================

1. [Install PlatformIO Core](http://docs.platformio.org/page/core.html)
2. Run these commands:

    ```bash
    # Change directory to example
    > cd platformio-examples/wiring-blink

    # Build project
    > platformio run

    # Upload firmware
    > platformio run --target upload

    # Build specific environment
    > platformio run -e uno

    # Upload firmware for the specific environment
    > platformio run -e uno --target upload

    # Clean build files
    > platformio run --target clean
    ```