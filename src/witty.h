
#ifndef WITTY_H
#define WITTY_H

#define LED_BUILTIN_R D8
#define LED_BUILTIN_G D6
#define LED_BUILTIN_B D7

#define LDR_BUILTIN A0

#endif // WITTY_H
