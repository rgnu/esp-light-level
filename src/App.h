#ifndef App_h
#define App_h

#include "PubSubClient.h"

#define LIGHT_LOW_TRH 50
#define LIGHT_NORMAL_TRH 100
#define LIGHT_HIGH_TRH 150

class App
{
  private:
    PubSubClient * mqttClient;
    String accountId;
    String clientId;
    int lightLevel;
    int lightState;
    int lightLevelLow;

  public:
    App(PubSubClient *mqttClient, String accountId, String clientId);

    String getAccountId();

    String getClientId();

    void setAlertState();

    void setLightLevel(int lightLevel);
    int getLightLevel();


    void setWarningState();
    void setNormalState();
    int lightLevelRead();
    int lightLevelState(int level);
    void lightLevelSetState(int state);
    void mqttHandleMessage(char *topic, unsigned char *payload, unsigned int length);
    void mqttSendStateMessage();
    void mqttReconnect();

    void loop();
};
#endif