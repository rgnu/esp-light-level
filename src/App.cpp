#include "Arduino.h"
#include "App.h"
#include "PubSubClient.h"
#include "witty.h"

App::App(PubSubClient *mqttClient, String accountId, String clientId)
{
    this->mqttClient = mqttClient;
    this->accountId = accountId;
    this->clientId = clientId;
    this->lightLevelLow = LIGHT_LOW_TRH;

    this->mqttClient->setCallback([this](char *topic, unsigned char *payload, unsigned int length) { this->mqttHandleMessage(topic, payload, length); });
}

String App::getAccountId()
{
    return this->accountId;
}

String App::getClientId()
{
    return this->clientId;
}

void App::setAlertState()
{
    digitalWrite(LED_BUILTIN_R, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN_R, LOW);
    digitalWrite(LED_BUILTIN_G, LOW);
    digitalWrite(LED_BUILTIN_B, LOW);
}

void App::setWarningState()
{
    digitalWrite(LED_BUILTIN_R, HIGH);
    digitalWrite(LED_BUILTIN_G, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN_R, LOW);
    digitalWrite(LED_BUILTIN_G, LOW);
    digitalWrite(LED_BUILTIN_B, LOW);
}

void App::setNormalState()
{
    digitalWrite(LED_BUILTIN_G, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN_R, LOW);
    digitalWrite(LED_BUILTIN_G, LOW);
    digitalWrite(LED_BUILTIN_B, LOW);
}

int App::lightLevelRead()
{
    return analogRead(LDR_BUILTIN);
}

int App::lightLevelState(int level)
{
    if (level < LIGHT_LOW_TRH)
        return 2;
    if (level < LIGHT_NORMAL_TRH)
        return 1;
    return 0;
}

void App::lightLevelSetState(int state)
{
    if (state == 0)
        return setNormalState();
    if (state == 1)
        return setWarningState();
    if (state == 2)
        return setAlertState();
}

void App::mqttHandleMessage(char *topic, unsigned char *payload, unsigned int length)
{
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    for (unsigned int i = 0; i < length; i++)
    {
        Serial.print((char)payload[i]);
    }
    Serial.println();
}

void App::mqttSendStateMessage()
{
    String payloadTpl = String("{\"name\":\"{{clientId}}\",\"fields\":{\"lightLevel\":{{lightLevel}},\"lightLevelLow\":{{lightLevelLow}},\"lightState\":{{lightState}}},\"tags\":{\"type\":\"lightLevel\"}}");
    String stateTopic = String("org/{{accountId}}/thing/{{clientId}}/state");

    stateTopic.replace("{{accountId}}", this->accountId);
    stateTopic.replace("{{clientId}}", this->clientId);

    String payload = payloadTpl;

    payload.replace("{{clientId}}", this->clientId);
    payload.replace("{{lightLevel}}", String(this->lightLevel));
    payload.replace("{{lightState}}", String(this->lightState));
    payload.replace("{{lightLevelLow}}", String(this->lightLevelLow));

    this->mqttClient->publish(stateTopic.c_str(), payload.c_str());
}

void App::mqttReconnect()
{

    String inTopic = String("org/{{accountId}}/thing/{{clientId}}/setProperty");
    String payload;

    inTopic.replace("{{accountId}}", this->accountId);
    inTopic.replace("{{clientId}}", this->clientId);

    // Loop until we're reconnected
    while (!this->mqttClient->connected())
    {
        Serial.print("Attempting MQTT connection...");

        this->setAlertState();

        if (this->mqttClient->connect(this->clientId.c_str()))
        {
            Serial.println("connected");
            this->mqttSendStateMessage();
            this->mqttClient->subscribe(inTopic.c_str(), 1);
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(this->mqttClient->state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

void App::loop()
{
    if (!this->mqttClient->connected())
    {
        this->mqttReconnect();
    }
    this->mqttClient->loop();

    this->lightLevel = this->lightLevelRead();
    this->lightState = this->lightLevelState(this->lightLevel);
    this->lightLevelSetState(this->lightState);

    Serial.print("Light Level: ");
    Serial.print(this->lightLevel);
    Serial.print(" State: ");
    Serial.println(this->lightState);

    this->mqttSendStateMessage();
    this->mqttClient->loop();
}