/*
 * Blink
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */
#include "Arduino.h"

#include <ESP8266WiFi.h> //https://github.com/esp8266/Arduino
//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager
#include <ESP8266mDNS.h>
#include <PubSubClient.h>

#include "App.h"

#include "witty.h"


String accountId = "e1bf8c06-a018-11e9-a2a3-2a2ae2dbcce4";
String clientId = "rgnu-" + String(ESP.getChipId());
const char *mqttServer = "broker.hivemq.com";

WiFiClient espClient;
PubSubClient client(espClient);

App app = App(&client, accountId, clientId);

void setup()
{
  Serial.begin(115200);
  pinMode(LED_BUILTIN_R, OUTPUT);
  pinMode(LED_BUILTIN_G, OUTPUT);
  pinMode(LED_BUILTIN_B, OUTPUT);

  Serial.println("Connecting ...");
  WiFiManager wifiManager;
  wifiManager.autoConnect(app.getClientId().c_str());

  if (!MDNS.begin(app.getClientId().c_str()))
  { // Start the mDNS responder for esp8266.local
    Serial.println("Error setting up MDNS responder!");
  }
  Serial.println("mDNS responder started");

  MDNS.addService("rgnu-light-level", "tcp", 80);

  client.setServer(mqttServer, 1883);
}

void loop()
{
  app.loop();
  WiFi.forceSleepBegin();
  delay(28000);
  WiFi.forceSleepWake();
  delay(2000);
}